"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Joke_1 = require("../entities/Joke");
exports.getRandomElement = (array) => {
    const randomElement = array[Math.floor(Math.random() * array.length)];
    return randomElement;
};
exports.stringIncludesCategory = (string) => {
    let category = '';
    if (string.includes('nerdy')) {
        category = 'nerdy';
    }
    else if (string.includes('explicit')) {
        category = 'explicit';
    }
    else if (string.includes('random')) {
        category = 'random';
    }
    else {
        category = 'none';
    }
    return category;
};
exports.capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};
exports.createJoke = (jokeRes) => {
    const { joke } = jokeRes.value;
    const wantedJoke = new Joke_1.Joke(joke);
    return wantedJoke.joke;
};
//# sourceMappingURL=helpers.js.map