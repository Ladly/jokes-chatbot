"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function undefinedAnswers(controller) {
    controller.hears(['yes', 'no', 'maybe'], 'message_received', (bot, message) => {
        bot.replyWithTyping(message, 'You\'ll have to give me more details');
    });
}
exports.undefinedAnswers = undefinedAnswers;
//# sourceMappingURL=index.js.map