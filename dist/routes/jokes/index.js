"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const JokesServices_1 = require("../../services/JokesServices");
const helpers_1 = require("../../utils/helpers");
function getOneJoke(controller) {
    controller.hears(['more', 'another', 'random'], 'message_received, facebook_postback', (bot, message) => {
        JokesServices_1.jokesServices.getApiJoke('random')
            .then(joke => bot.replyWithTyping(message, joke));
    });
}
exports.getOneJoke = getOneJoke;
function getJoke(controller) {
    controller.hears(['joking', 'joke', 'jest', 'fun'], 'message_received', (bot, message) => {
        bot.startConversation(message, (err, convo) => {
            convo.say('Sure! Do you want random joke or specific one from some of categories:');
            convo.ask('nerdy, explicit, random or none', (response, convo) => {
                const userSelectedCategory = helpers_1.stringIncludesCategory(response.text); // since I have category check in two modules i put check function in helpers.
                if (userSelectedCategory !== 'none') {
                    JokesServices_1.jokesServices.getApiJoke(userSelectedCategory)
                        .then(joke => {
                        convo.say(joke);
                        convo.next();
                    });
                }
                else {
                    convo.say('Why? Do you want to hear a prank?');
                    convo.next();
                }
            });
        });
    });
}
exports.getJoke = getJoke;
//# sourceMappingURL=index.js.map