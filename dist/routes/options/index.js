"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("./helpers");
function options(controller) {
    controller.hears(['menu', 'options'], 'message_received', (bot, message) => {
        bot.replyWithTyping(message, {
            attachment: helpers_1.menuAttachment,
        });
    });
}
exports.options = options;
function help(controller) {
    const userQuestionMessages = ['help', 'info'];
    controller.hears(userQuestionMessages, 'message_received, facebook_postback', function (bot, message) {
        bot.replyWithTyping(message, 'List of commands:\n menu,\n help,\n prank,\n joke,\n hi,\n hello,\n good day,\n gig,\n ok,\n thanks,\n bye.\n');
    });
}
exports.help = help;
//# sourceMappingURL=index.js.map