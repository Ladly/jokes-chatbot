"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../../utils/helpers");
const helpers_2 = require("./helpers");
function endConversation(controller) {
    const userEndConversation = ['ok', 'thanks', 'great', 'bye'];
    const botReplayMessages = ['Anytime! Happy to make you day ...', 'Happy to help ...'];
    const randomBotReplyMessage = helpers_1.getRandomElement(botReplayMessages);
    controller.hears(userEndConversation, 'message_received', (bot, message) => {
        bot.startConversation(message, (err, convo) => {
            convo.say(randomBotReplyMessage);
            convo.say({ attachment: helpers_2.endConversationAttachment });
            convo.next();
        });
    });
}
exports.endConversation = endConversation;
//# sourceMappingURL=index.js.map