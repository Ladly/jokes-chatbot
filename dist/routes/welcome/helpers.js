"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.startConversationAttachment = {
    'type': 'template',
    'payload': {
        'template_type': 'generic',
        'elements': [
            {
                'title': 'Hello, nice to meet you',
                'image_url': 'https://image.ibb.co/fyQh3p/06dc051c6d6f1fe3645a7ded30f559c7.png',
                'buttons': [
                    {
                        'type': 'postback',
                        'title': 'Would you like a joke?',
                        'payload': 'joke'
                    },
                    {
                        'type': 'postback',
                        'title': 'Maybe prank?',
                        'payload': 'prank'
                    },
                    {
                        'type': 'postback',
                        'title': 'See my other skills?',
                        'payload': 'info'
                    }
                ]
            },
        ]
    }
};
//# sourceMappingURL=helpers.js.map