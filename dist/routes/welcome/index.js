"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../../utils/helpers");
const helpers_2 = require("./helpers");
function welcome(controller) {
    const userStartingMessages = ['hello', 'hi', 'good day'];
    const botReplayMessages = ['Hi there let me introduce myself my name is Something Catchy :)', 'How can I help you?', 'You look sad- want me to tell you a joke?'];
    const randomBotReplyMessage = helpers_1.getRandomElement(botReplayMessages);
    controller.hears(userStartingMessages, 'message_received, facebook_postback', function (bot, message) {
        bot.startConversation(message, (err, convo) => {
            convo.say(randomBotReplyMessage);
            convo.say({ attachment: helpers_2.startConversationAttachment });
            convo.next();
        });
    });
}
exports.welcome = welcome;
//# sourceMappingURL=index.js.map