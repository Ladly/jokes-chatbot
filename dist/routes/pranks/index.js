"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../../utils/helpers");
const helpers_2 = require("./helpers");
function getPrank(controller) {
    let userSelectedCategory = ''; // since i lose scope after next() I had to 'hoist' category (have to check if there is something better i think i saw it)
    controller.hears(['prank', 'gig'], 'message_received, facebook_postback', (bot, message) => {
        bot.startConversation(message, (err, convo) => {
            convo.ask('Sure! Choose a category for a prank: \nnerdy, explicit, random or none?', (response, convo) => {
                userSelectedCategory = helpers_1.stringIncludesCategory(response.text);
                convo.next();
                if (userSelectedCategory !== 'none') {
                    convo.ask('What\'s your friend first name?', (response, convo) => {
                        let userSelectedName = helpers_1.capitalizeFirstLetter(response.text);
                        convo.next();
                        if (typeof userSelectedName === 'number') {
                            convo.ask('This does not look like a real name let\'s try again. What\'s your friend first name?', (response, convo) => {
                                userSelectedName = helpers_1.capitalizeFirstLetter(response.text);
                                convo.next();
                                helpers_2.conversationEnd(convo, userSelectedName, userSelectedCategory);
                            });
                        }
                        else {
                            helpers_2.conversationEnd(convo, userSelectedName, userSelectedCategory);
                        }
                    });
                }
                else {
                    convo.say('Why? Do you want to hear a joke?');
                    convo.next();
                }
            });
        });
    });
}
exports.getPrank = getPrank;
//# sourceMappingURL=index.js.map