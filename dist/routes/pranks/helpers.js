"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const JokesServices_1 = require("../../services/JokesServices");
const helpers_1 = require("../../utils/helpers");
exports.conversationEnd = (convo, userSelectedName, userSelectedCategory) => {
    convo.ask('What\'s your friend last name?', (response, convo) => {
        const userSelectedLastname = helpers_1.capitalizeFirstLetter(response.text);
        convo.next();
        JokesServices_1.jokesServices.getApiJokeWithName(userSelectedName, userSelectedLastname, userSelectedCategory)
            .then(joke => {
            convo.say(`Here is ${userSelectedCategory} joke for ${userSelectedName}: ${joke}`);
            convo.next();
        });
    });
};
//# sourceMappingURL=helpers.js.map