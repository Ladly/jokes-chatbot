"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = __importDefault(require("node-fetch"));
const constants_1 = require("../utils/constants");
const helpers_1 = require("../utils/helpers");
class JokesServices {
    constructor() {
        this.getApiJoke = (category = 'random') => {
            let wantedUrl = '';
            if (category === 'random') {
                wantedUrl = `${constants_1.CHUCK_NORIS_URL}/${category}/`;
            }
            else {
                wantedUrl = `${constants_1.CHUCK_NORIS_URL}/random?limitTo=[${category}]`;
            }
            return this.fetchJoke(wantedUrl);
        };
        this.getApiJokeWithName = (name, lastname, category) => {
            let wantedUrl = '';
            if (category === 'random') {
                wantedUrl = `${constants_1.CHUCK_NORIS_URL}/${category}?firstName=${name}&lastName=${lastname}`;
            }
            else {
                wantedUrl = `${constants_1.CHUCK_NORIS_URL}/random?firstName=${name}&lastName=${lastname}&limitTo=[${category}]`;
            }
            return this.fetchJoke(wantedUrl);
        };
    }
    fetchJoke(wantedUrl) {
        return node_fetch_1.default(wantedUrl)
            .then(res => res.json())
            .then(res => helpers_1.createJoke(res))
            .catch(error => console.log(error));
    }
}
exports.jokesServices = new JokesServices;
//# sourceMappingURL=JokesServices.js.map