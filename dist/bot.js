"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const botkit_1 = __importDefault(require("botkit"));
const dotenv_1 = __importDefault(require("dotenv"));
const welcome_1 = require("./routes/welcome");
const jokes_1 = require("./routes/jokes");
const pranks_1 = require("./routes/pranks");
const options_1 = require("./routes/options");
const endConversation_1 = require("./routes/endConversation");
const undefinedAnswers_1 = require("./routes/undefinedAnswers");
dotenv_1.default.config();
const createBotkitController = () => {
    return botkit_1.default.facebookbot({
        access_token: process.env.FACEBOOK_PAGE_ACCESS_TOKEN,
        verify_token: process.env.VERIFY_TOKEN,
        require_delivery: true,
        receive_via_postback: true // this is used for allowing facebook_postbacks to trigger messsage_recived
    });
};
const controller = createBotkitController();
const bot = controller.spawn({});
controller.setupWebserver((process.env.PORT || 5000), (err, webserver) => {
    controller.createWebhookEndpoints(webserver, bot, () => {
        console.log('bot connected');
    });
});
welcome_1.welcome(controller);
jokes_1.getJoke(controller);
jokes_1.getOneJoke(controller);
pranks_1.getPrank(controller);
options_1.options(controller);
options_1.help(controller);
endConversation_1.endConversation(controller);
undefinedAnswers_1.undefinedAnswers(controller);
//# sourceMappingURL=bot.js.map