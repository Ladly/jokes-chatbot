# Jokes chatbot
Facebook chatbot which will tell jokes as respones.

[Facebook page](https://www.facebook.com/Vuks-joke-chatbot-2068682763154661).

## Getting Started

Get project to your local machine: git clone https://gitlab.com/Ladly/jokes-chatbot.git.

Install dependencies: yarn install.

For development use: yarn run dev.

For production use: yarn run prod.

Important to notice 1: for development you need [ngrok](https://ngrok.com/) or some other tunneling tool.

Important to notice 2: .env is mandatory in this setup. For now it must have: 

- FACEBOOK_PAGE_ACCESS_TOKEN=(get this when create facebook app)
- VERIFY_TOKEN=(whatever you like- only important is to be same on facebook and in you're app for verification)

## Infrastructure decisions and limitations

Since this wasn't made for production, I hosted it for free on heroku which has a delay when inactive for hour. 
If this was an app for production, I would have selected some better/paid option (paid heroku, AWS, etc).

Used ngrok as tunneling tool because of it's great documentation.

Used node-fetch because I've done some extensive front-end work and I am used to using fetch even though request would do the job.

I used this sturcture because it is easy to follow and very scalable.

More details about the work done can be found on trello link below.

# Project management:

I used [trello](https://trello.com/b/lKzNxCcF).

## Authors

* **Vuk Ivanovic** - [Ladly](https://github.com/Ladly).

## License

This project is licensed under the MIT License.



