import { stringIncludesCategory, capitalizeFirstLetter } from '../../utils/helpers'
import { conversationEnd } from './helpers'


export function getPrank (controller: any) {

	let userSelectedCategory = '' // since i lose scope after next() I had to 'hoist' category (have to check if there is something better i think i saw it)

	controller.hears(['prank', 'gig'], 'message_received, facebook_postback', (bot: any, message: any) => {
		bot.startConversation(message, (err: Error, convo: any) => {
			convo.ask('Sure! Choose a category for a prank: \nnerdy, explicit, random or none?', (response: any, convo: any) => {
				userSelectedCategory = stringIncludesCategory(response.text)
				convo.next()
					if (userSelectedCategory !== 'none') {
						convo.ask('What\'s your friend first name?', (response: any, convo: any) => {
						let userSelectedName = capitalizeFirstLetter(response.text)
						convo.next()
						if (typeof userSelectedName === 'number') {
							convo.ask('This does not look like a real name let\'s try again. What\'s your friend first name?', (response: any, convo: any) => {
								userSelectedName = capitalizeFirstLetter(response.text)
								convo.next()
								conversationEnd(convo, userSelectedName, userSelectedCategory)
							})
						} else {
							conversationEnd(convo, userSelectedName, userSelectedCategory)
						}
					})
				} else {
					convo.say('Why? Do you want to hear a joke?')
					convo.next()
				}
			})
		})
	})
}






