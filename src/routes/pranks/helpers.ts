import { jokesServices } from '../../services/JokesServices'
import { capitalizeFirstLetter } from '../../utils/helpers'


export const conversationEnd = (convo: any, userSelectedName: string, userSelectedCategory: string) => {
	convo.ask('What\'s your friend last name?', (response: any, convo: any) => {
		const userSelectedLastname = capitalizeFirstLetter(response.text)
		convo.next()
		jokesServices.getApiJokeWithName(userSelectedName, userSelectedLastname, userSelectedCategory)
		.then(joke => {
			convo.say(`Here is ${userSelectedCategory} joke for ${userSelectedName}: ${joke}`)
			convo.next()
		})
	})
}