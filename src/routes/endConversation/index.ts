import { getRandomElement } from '../../utils/helpers'
import { endConversationAttachment } from './helpers'

export function endConversation (controller: any) {
	const userEndConversation = ['ok', 'thanks', 'great', 'bye']
	const botReplayMessages = ['Anytime! Happy to make you day ...', 'Happy to help ...']

	const randomBotReplyMessage = getRandomElement(botReplayMessages)

	controller.hears(userEndConversation, 'message_received', (bot: any, message: any) => {
		bot.startConversation(message, (err: Error, convo: any) => {
		convo.say(randomBotReplyMessage)
		convo.say( { attachment: endConversationAttachment })
		convo.next()
		})
	})
}


