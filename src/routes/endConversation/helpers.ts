export const endConversationAttachment = {
	'type': 'template',
	'payload': {
		'template_type': 'generic',
		'elements': [
			{
				'title': 'You dont have to go :(',
				'image_url': 'https://image.ibb.co/hB5nZU/bf1dcff2ad08adf4a24ca6c92384a383.png',
				'buttons': [
					{
					'type': 'postback',
					'title' : 'Another joke?',
					'payload': 'joke'
					},
					{
					'type': 'postback',
					'title' : 'Maybe prank?',
					'payload': 'prank'
					},
					{
					'type': 'postback',
					'title' : 'See my other skills?',
					'payload': 'info'
					}
				]
			},
		]
	}
}