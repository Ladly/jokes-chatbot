export function undefinedAnswers (controller: any) {
	controller.hears(['yes', 'no', 'maybe'], 'message_received', (bot: any, message: any) => {
		bot.replyWithTyping(message, 'You\'ll have to give me more details')
	})
}