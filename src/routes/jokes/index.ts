import { jokesServices } from '../../services/JokesServices'
import { stringIncludesCategory } from '../../utils/helpers'


export function getOneJoke (controller: any) {
	controller.hears(['more', 'another', 'random'], 'message_received, facebook_postback', (bot: any, message: any) => {
		jokesServices.getApiJoke('random')
			.then(joke => bot.replyWithTyping(message, joke))
	})
}


export function getJoke (controller: any) {
	controller.hears(['joking', 'joke', 'jest', 'fun'], 'message_received', (bot: any, message: any) => {
		bot.startConversation(message, (err: Error, convo: any) => {
			convo.say('Sure! Do you want random joke or specific one from some of categories:')
			convo.ask('nerdy, explicit, random or none', (response: any, convo: any) => {
				const userSelectedCategory = stringIncludesCategory(response.text) // since I have category check in two modules i put check function in helpers.
				if (userSelectedCategory !== 'none') {
					jokesServices.getApiJoke(userSelectedCategory)
						.then(joke => {
							convo.say(joke)
							convo.next()
						})
				} else {
					convo.say('Why? Do you want to hear a prank?')
					convo.next()
				}
			})
		})
	})
}
