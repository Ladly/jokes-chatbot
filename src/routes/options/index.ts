import { menuAttachment } from './helpers'


export function options (controller: any) {
	controller.hears(['menu', 'options'], 'message_received', (bot: any, message: any) => {
		bot.replyWithTyping(message, {
			attachment: menuAttachment,
		})
	})
}


export function help (controller: any) {
	const userQuestionMessages = ['help', 'info']

	controller.hears(userQuestionMessages, 'message_received, facebook_postback', function(bot: any, message: Object) {
		bot.replyWithTyping(message, 'List of commands:\n menu,\n help,\n prank,\n joke,\n hi,\n hello,\n good day,\n gig,\n ok,\n thanks,\n bye.\n')
	})
}

