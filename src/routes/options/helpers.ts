export const menuAttachment = {
	'type': 'template',
	'payload': {
		'template_type': 'generic',
		'elements': [
			{
				'title': 'Most used commands',
				'image_url': 'https://image.ibb.co/gj0beU/icon-ios7-gear-128.png',
				'buttons': [
					{
					'type': 'postback',
					'title' : 'Tell me a joke',
					'payload': 'joke'
					},
					{
					'type': 'postback',
					'title' : 'Tell me a prank',
					'payload': 'prank'
					},
					{
					'type': 'postback',
					'title' : 'Help',
					'payload': 'info'
					}
				]
			},
		]
	}
}