import { getRandomElement } from '../../utils/helpers'
import { startConversationAttachment } from './helpers'

export function welcome (controller: any) {
	const userStartingMessages = ['hello', 'hi', 'good day']
	const botReplayMessages = ['Hi there let me introduce myself my name is Something Catchy :)', 'How can I help you?', 'You look sad- want me to tell you a joke?']

	const randomBotReplyMessage = getRandomElement(botReplayMessages)

	controller.hears(userStartingMessages, 'message_received, facebook_postback', function(bot: any, message: Object) {
		bot.startConversation(message, (err: Error, convo: any) => {
			convo.say(randomBotReplyMessage)
			convo.say({ attachment: startConversationAttachment })
			convo.next()
		})
	})
}

