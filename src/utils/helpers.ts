import { Joke } from '../entities/Joke'

export const getRandomElement = (array: string[]) => {
	const randomElement = array[ Math.floor(Math.random() * array.length) ]

	return randomElement
}

export const stringIncludesCategory = (string: string) => {
	let category = ''

	if (string.includes('nerdy')) {
		category = 'nerdy'
	} else if (string.includes('explicit')) {
		category = 'explicit'
	} else if (string.includes('random')) {
		category = 'random'
	} else {
		category = 'none'
	}

	return category
}

export const capitalizeFirstLetter = (string: string) => {

	return string.charAt(0).toUpperCase() + string.slice(1)
}

interface Value {
	id: string
	joke: string
}

export const createJoke = ( jokeRes: {value: Value} ) => {
	const { joke } = jokeRes.value
	const wantedJoke = new Joke(joke)

	return wantedJoke.joke
}