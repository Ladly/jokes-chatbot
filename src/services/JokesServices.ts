import fetch from 'node-fetch'
import { CHUCK_NORIS_URL } from '../utils/constants'
import { createJoke } from '../utils/helpers'

class JokesServices {

	public getApiJoke = (category: string = 'random') => {
		let wantedUrl = ''
		if (category === 'random') {
			wantedUrl = `${CHUCK_NORIS_URL}/${category}/`
		} else {
			wantedUrl = `${CHUCK_NORIS_URL}/random?limitTo=[${category}]`
		}

		return this.fetchJoke(wantedUrl)
	}

	public getApiJokeWithName = (name: string, lastname: string, category: string) => {
		let wantedUrl = ''
		if (category === 'random') {
			wantedUrl = `${CHUCK_NORIS_URL}/${category}?firstName=${name}&lastName=${lastname}`
		} else {
			wantedUrl = `${CHUCK_NORIS_URL}/random?firstName=${name}&lastName=${lastname}&limitTo=[${category}]`
		}

		return this.fetchJoke(wantedUrl)
	}

	private fetchJoke (wantedUrl: string) {
		return fetch(wantedUrl)
			.then(res => res.json())
			.then(res =>  createJoke(res))
			.catch(error => console.log(error))
	}
}


export const jokesServices = new JokesServices
