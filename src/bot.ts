import Botkit from 'botkit'
import dotenv from 'dotenv'
import { welcome } from './routes/welcome'
import { getJoke, getOneJoke } from './routes/jokes'
import { getPrank } from './routes/pranks'
import { options, help } from './routes/options'
import { endConversation } from './routes/endConversation'
import { undefinedAnswers } from './routes/undefinedAnswers'

dotenv.config()

const createBotkitController = () => {
	return Botkit.facebookbot({
		access_token: process.env.FACEBOOK_PAGE_ACCESS_TOKEN,
		verify_token: process.env.VERIFY_TOKEN,
		require_delivery: true,
		receive_via_postback: true // this is used for allowing facebook_postbacks to trigger messsage_recived
	})
}
const controller = createBotkitController()

const bot = controller.spawn({})

controller.setupWebserver((process.env.PORT || 5000), (err: Error, webserver: Object) => {
	controller.createWebhookEndpoints(webserver, bot, () => {
		console.log('bot connected')
	})
})

welcome(controller)
getJoke(controller)
getOneJoke(controller)
getPrank(controller)
options(controller)
help(controller)
endConversation(controller)
undefinedAnswers(controller)











